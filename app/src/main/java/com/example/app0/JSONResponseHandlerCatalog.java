package com.example.app0;

import android.util.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;



/**
 * Process the response to a GET request to the Web service
 * https://demo-lia.univ-avignon.fr/cerimuseum/catalog
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerCatalog {

    private static final String TAG = JSONResponseHandlerCatalog.class.getSimpleName();

    private JSONResponseHandlerArtefact jsonItem;
    private Artefact tmpArtefact;
    private ArrayList<Artefact> list;


    public JSONResponseHandlerCatalog() {
        this.list = new ArrayList<>();
        this.tmpArtefact = new Artefact();
    }

    public ArrayList<Artefact> getArtefacts() { return this.list; }


    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readCatalog(reader);
        } finally {
            reader.close();
        }
    }

    public void readCatalog(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String webId = reader.nextName();
            this.tmpArtefact = new Artefact();
            this.tmpArtefact.setId(webId);
            this.jsonItem = new JSONResponseHandlerArtefact(this.tmpArtefact);
            this.jsonItem.readItem(reader);
            this.list.add(this.tmpArtefact);
        }
        reader.endObject();
    }
}