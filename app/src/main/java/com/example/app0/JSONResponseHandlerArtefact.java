package com.example.app0;

import android.util.JsonReader;
import android.util.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 *
 * https://demo-lia.univ-avignon.fr/cerimuseum/items/..itemWebId..
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerArtefact {

    private static final String TAG = JSONResponseHandlerArtefact.class.getSimpleName();

    private Artefact artefact;


//    On suppose que l'artefact qui est passé possède un web id
    public JSONResponseHandlerArtefact(Artefact artefact) {
        this.artefact = artefact;
        this.artefact.clearAllLists();
        try {
            this.artefact.setphoto(WebServiceUrl.buildSearchThumbnail(this.artefact.getId()).toString());
        } catch (MalformedURLException e) { e.printStackTrace(); }
        this.artefact.setLastUpdate();
    }

    public Artefact getArtefact() { return this.artefact; }




    public void readItem(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if(name.equals("categories") && reader.peek() != JsonToken.NULL) {
                readStringArray(reader, this.artefact.getCategories());
            }
            else if(name.equals("technicalDetails") && reader.peek() != JsonToken.NULL) {
                readStringArray(reader, this.artefact.getTechDetails());
            }
            else if(name.equals("timeFrame") && reader.peek() != JsonToken.NULL) {
                readIntArray(reader, this.artefact.getTimeFrame());
            }
            else if(name.equals("pictures") && reader.peek() != JsonToken.NULL) {
                readImages(reader, this.artefact.getPictures());
            }
            else {
                readValue(reader, name);
            }
        }
        reader.endObject();

        if(this.artefact.getannee() == Artefact.NULL_YEAR && this.artefact.getTimeFrame().size() > 0)
            this.artefact.setannee(this.artefact.getTimeFrame().get(0));
    }


    private void readStringArray(JsonReader reader, ArrayList<String> l) throws IOException {
        reader.beginArray();
        while (reader.hasNext() ) {
            while (reader.hasNext()) {
                l.add(reader.nextString());
            }
        }
        reader.endArray();
    }

    private void readIntArray(JsonReader reader, ArrayList<Integer> l) throws IOException {
        reader.beginArray();
        while (reader.hasNext() ) {
            while (reader.hasNext()) {
                l.add(reader.nextInt());
            }
        }
        reader.endArray();
    }

    private void readImages(JsonReader reader, ArrayList<Image> l) throws IOException {
        reader.beginObject();
            while (reader.hasNext()) {
                String imageUrl = WebServiceUrl.buildSearchImage(this.artefact.getId(), reader.nextName()).toString();
                String description = reader.nextString();
                l.add(new Image(description, imageUrl));
            }
        reader.endObject();
    }

    private void readValue(JsonReader reader, String name) throws IOException {
        if(name.equals("description")) {
            if(reader.peek() == JsonToken.NULL) {
                this.artefact.setDesc("");
                reader.nextNull();
            }
            else
                this.artefact.setDesc(reader.nextString());
        }
        else if(name.equals("brand")) {
            if(reader.peek() == JsonToken.NULL) {
                this.artefact.setmarque("");
                reader.nextNull();
            }
            else
                this.artefact.setmarque(reader.nextString());
        }
        else if(name.equals("name") && reader.peek() != JsonToken.NULL) {
            this.artefact.setnom(reader.nextString());
        }
        else if(name.equals("year")) {
            if(reader.peek() == JsonToken.NULL) {
                this.artefact.setannee(1);
                reader.nextNull();
            }
            else
                this.artefact.setannee(reader.nextInt());
        }
        else {
            reader.skipValue();
        }
    }

}