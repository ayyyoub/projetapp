package com.example.app0;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>   {
    private static final String TAG = RecyclerViewAdapter.class.getName();

    private Context context;
    public List<Artefact> catalog;
    public List<Artefact> full;


    public RecyclerViewAdapter(Context context, List<Artefact> catalog) {
        this.context = context;
        this.catalog = catalog;
        ArrayList<Artefact> full = new ArrayList<>();

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        final Artefact artefact = this.catalog.get(position);

        ((ViewHolder) holder).artefactName.setText(artefact.getnom());
        ((ViewHolder) holder).idBdd = artefact.getColumnId();
        String categories = "";
        if (!artefact.getCategories().isEmpty()) {
            int i = 0;
            for (; i < artefact.getCategories().size() - 1; ++i)
                categories += artefact.getCategories().get(i) + ", ";
            categories += artefact.getCategories().get(i);
        }
        ((ViewHolder) holder).artefactMarque.setText(categories);
        Glide.with(((ViewHolder) holder).itemView)
                .load(artefact.getphoto())
                .centerCrop()
                .into(((ViewHolder) holder).artefactImage);
            // passer au ArtefactActivity
        ((ViewHolder) holder).Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ArtefactActivity.class);
                intent.putExtra(Artefact.TAG, artefact);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.catalog.size();
    }

    public void sortArtefactAlphabetic() {
        Collections.sort(this.catalog, new Comparator<Artefact>() {
            @Override
            public int compare(Artefact i1, Artefact i2) {
                return i1.getnom().compareTo(i2.getnom());
            }
        });
    }

    public void sortArtefactChronologically() {
        Collections.sort(this.catalog, new Comparator<Artefact>() {
            @Override
            public int compare(Artefact i1, Artefact i2) {
                Integer i1_year = new Integer(i1.getannee());
                Integer i2_year = new Integer(i2.getannee());
                return i1_year.compareTo(i2_year);
            }
        });
    }






    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView artefactImage;
        TextView artefactName;
        TextView artefactMarque;
        LinearLayout Layout;
        long idBdd;

        public ViewHolder(View itemView) {
            super(itemView);
            artefactImage = itemView.findViewById(R.id.artefactImage);
            artefactName = itemView.findViewById(R.id.artefactName);
            artefactMarque = itemView.findViewById(R.id.artefactMarque);
            Layout = itemView.findViewById(R.id.Layout);
        }
    }
}
