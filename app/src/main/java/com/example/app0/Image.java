package com.example.app0;

import android.os.Parcel;
import android.os.Parcelable;

public class Image implements Parcelable {


    private String desc;
    private String Url;

    Image(String description, String imageUrl)

    {
        this.desc = description;
        this.Url = imageUrl;
    }

    protected Image(Parcel in) {
        desc = in.readString();
        Url = in.readString();
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public String getDescription() {
        return this.desc;
    }

    public String getImageUrl() {
        return this.Url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.desc);
        dest.writeString(this.Url);
    }
}
