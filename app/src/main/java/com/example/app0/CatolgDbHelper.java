package com.example.app0;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class CatolgDbHelper extends SQLiteOpenHelper {


    private static final String TAG = CatolgDbHelper.class.getSimpleName();

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "museum.db";

    public static final String TABLE_NAME = "catalog";

    public static final String _ID = "_id";
    public static final String COLUMN_ID = "w_id";
    public static final String COLUMN_NOM = "nom";
    public static final String COLUMN_PHOTO = "photo";
    public static final String COLUMN_MARQUE = "marque";
    public static final String COLUMN_ANNEE = "annee";
    public static final String COLUMN_TIME_FRAME = "time_frame";
    public static final String COLUMN_CATEGORIES = "categories";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_PICTURES = "pictures";
    public static final String COLUMN_TECHNICAL_DETAILS = "technical_details";
    public static final String COLUMN_LAST_UPDATE = "last_update";

    public CatolgDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_BOOK_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_ID + " TEXT NOT NULL, " +
                COLUMN_NOM + " TEXT NOT NULL, " +
                COLUMN_PHOTO + " TEXT, " +
                COLUMN_MARQUE + " TEXT, " +
                COLUMN_ANNEE + " INTEGER, " +
                COLUMN_TIME_FRAME + " TEXT, " +
                COLUMN_CATEGORIES + " TEXT, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_PICTURES + " TEXT, " +
                COLUMN_TECHNICAL_DETAILS + " INTEGER, " +
                COLUMN_LAST_UPDATE + " TEXT, " +
                // To assure the application have just one artefact entry per
                // artefact name and brand, it's created a UNIQUE
                " UNIQUE (" + COLUMN_NOM + ", " +
                COLUMN_MARQUE + ") ON CONFLICT ROLLBACK);";

        db.execSQL(SQL_CREATE_BOOK_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

     //  Fills ContentValues result from an Artefact object

    private ContentValues fill(Artefact artefact) {
        Gson gson = new Gson();
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID, artefact.getId());
        values.put(COLUMN_NOM, artefact.getnom());
        values.put(COLUMN_PHOTO, artefact.getphoto());
        values.put(COLUMN_MARQUE, artefact.getmarque());
        values.put(COLUMN_ANNEE, artefact.getannee());
        values.put(COLUMN_TIME_FRAME, gson.toJson(artefact.getTimeFrame()));
        values.put(COLUMN_CATEGORIES, gson.toJson(artefact.getCategories()));
        values.put(COLUMN_DESCRIPTION, artefact.getDesc());

        values.put(COLUMN_PICTURES, gson.toJson(artefact.getPictures()));///

        values.put(COLUMN_TECHNICAL_DETAILS, gson.toJson(artefact.getTechDetails()));
        values.put(COLUMN_LAST_UPDATE, artefact.getLastUpdate());
        return values;
    }


     // return true if the artefact was added to the table ; false otherwise (case when the pair (name, brand) is

    public boolean addArtefact(Artefact artefact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = fill(artefact);



        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);
        db.close();

        return (rowID != -1);
    }




     // Returns a cursor on all the items of the data base

    public Cursor fetchAllArtefact() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_NOM + " ASC", null);


        return cursor;
    }



    public Cursor fetchCategories() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, null,
                null, null, null, null, COLUMN_CATEGORIES + " ASC", null);

        return cursor;
    }

    /**
     * Returns a list on all the items of the data base
     */
    public List<Artefact> getAllArtefact() {
        List<Artefact> res = new ArrayList<>();
        Cursor c = this.fetchAllArtefact();
        while (c.moveToNext()) {
            res.add(this.cursorToArtefact(c));
        }
        c.close();

        return res;
    }



    public List<String> getCategories() {
        Gson gson = new Gson();
        List<String> res = new ArrayList<>();
        Cursor c = this.fetchCategories();
        while (c.moveToNext()) {
            ArrayList<String> categories = gson.fromJson(c.getString(c.getColumnIndex(COLUMN_CATEGORIES)), ArrayList.class);
            for (String str : categories)
                res.add(str);
        }
        c.close();

        List<String> distinctRes = new ArrayList<>();
        distinctRes.addAll(new HashSet<>(res));
        Collections.sort(distinctRes, new Comparator<String>() {
            @Override
            public int compare(String str1, String str2) {
                return str1.toLowerCase().compareTo(str2.toLowerCase());
            }
        });

        return distinctRes;
    }

    public void synchronize(ArrayList<Artefact> catalog) {
        this.deleteAllArtefact();
        for (Artefact artefact : catalog)
            this.addArtefact(artefact);
    }

    public void deleteAllArtefact() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
        db.close();
    }



    public Artefact cursorToArtefact(Cursor cursor) {

        Gson gson = new Gson();
        ArrayList<Integer> timeFrames = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_TIME_FRAME)), ArrayList.class);
        ArrayList<String> categories = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_CATEGORIES)), ArrayList.class);


        Type listType = new TypeToken<ArrayList<Image>>() {
        }.getType();
        ArrayList<Image> pictures = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_PICTURES)), listType);///


        ArrayList<String> technicalDetails = gson.fromJson(cursor.getString(cursor.getColumnIndex(COLUMN_TECHNICAL_DETAILS)), ArrayList.class);

        Artefact artefact = new Artefact(cursor.getLong(cursor.getColumnIndex(_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_NOM)),
                cursor.getString(cursor.getColumnIndex(COLUMN_PHOTO)),
                cursor.getString(cursor.getColumnIndex(COLUMN_MARQUE)),
                cursor.getInt(cursor.getColumnIndex(COLUMN_ANNEE)),
                timeFrames,
                categories,
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                pictures,
                technicalDetails,
                cursor.getString(cursor.getColumnIndex(COLUMN_LAST_UPDATE))
        );

        return artefact;
    }





    public boolean addArtefactToRechercheTable(Artefact artefact) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = fill(artefact);

        // insérer la ligne

        long rowID = db.insertWithOnConflict(TABLE_NAME, null, values, CONFLICT_IGNORE);

        return (rowID != -1);
    }

    private void fillTableRecherche(String queryParam) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Artefact> listofArtefact = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE (" + COLUMN_NOM + " LIKE ?) " +
                "OR (" + COLUMN_MARQUE + " LIKE ?) " +
                "OR (" + COLUMN_CATEGORIES + " LIKE ?) " +
                "OR (" + COLUMN_ID + " LIKE ?) " +
                "OR (" + COLUMN_TIME_FRAME + " LIKE ?) " +
                "OR (" + COLUMN_DESCRIPTION + " LIKE ?) " +
                "OR (" + COLUMN_ANNEE + " LIKE ?) " +
                "OR (" + COLUMN_TECHNICAL_DETAILS + " LIKE ?) " +
                "OR (" + COLUMN_LAST_UPDATE + " LIKE ?) ", new String[]{queryParam, queryParam, queryParam, queryParam, queryParam, queryParam, queryParam, queryParam, queryParam});
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                cursor.moveToFirst();
                listofArtefact.add(cursorToArtefact(cursor));
            }
        }

        while (cursor.moveToNext()) {
            listofArtefact.add(cursorToArtefact(cursor));
        }
        for (int i = 0; i < listofArtefact.size(); i++) {
            addArtefactToRechercheTable(listofArtefact.get(i));
        }
    }

    public List<Artefact> fetchResultOfSearch(String recherche) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Artefact> res = new ArrayList<>();

        String queryParam = "%" + recherche + "%";
        fillTableRecherche(queryParam);
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);

        while (cursor.moveToFirst()) {
            res.add(cursorToArtefact(cursor));
        }
        db.close();
        return res;
    }


}