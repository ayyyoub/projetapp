package com.example.app0;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class SearchAdapter extends RecyclerViewAdapter implements Filterable {

    private List<Artefact> result;
    private SearchFilter filter;

    public SearchAdapter(Context context, List<Artefact> catalog) {
        super(context, catalog);

        this.result = new ArrayList<>(this.catalog);
        this.filter = new SearchFilter();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Artefact artefact = this.result.get(position);

        ((ViewHolder) holder).artefactName.setText(artefact.getnom());
        ((ViewHolder)holder).idBdd = artefact.getColumnId();

        String categories = "";
        if(!artefact.getCategories().isEmpty()) {
            int i = 0;
            for(; i < artefact.getCategories().size() - 1 ; ++i)
                categories += artefact.getCategories().get(i) + ", ";
            categories += artefact.getCategories().get(i);
        }
        ((ViewHolder) holder).artefactMarque.setText(categories);



        Glide.with(((ViewHolder)holder).itemView)
                .load(artefact.getphoto())
                .centerCrop()
                .into(((ViewHolder)holder).artefactImage);

        ((ViewHolder) holder).Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Clicked on id " + artefact.getId() + "  ;  " + artefact.getnom());
                Intent intent = new Intent(v.getContext(), ArtefactActivity.class);
                intent.putExtra(Artefact.TAG, artefact);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.result.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    class SearchFilter extends Filter {
    // filtrage via les données de artefact
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Artefact> filteredList = new ArrayList<>();
            List<Artefact> priority = new ArrayList<>();

            if(constraint == null || constraint.length() == 0)
                filteredList.addAll(SearchAdapter.this.catalog);
            else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(Artefact artefact : SearchAdapter.this.catalog) {
                    if(artefact.getnom().toLowerCase().startsWith(filterPattern)) {
                        priority.add(artefact);
                        continue;
                    }
                    if(artefact.getmarque().toLowerCase().startsWith(filterPattern)) {
                        filteredList.add(artefact);
                        continue;
                    }
                    if(String.valueOf(artefact.getannee()).startsWith(filterPattern)) {
                        filteredList.add(artefact);
                        continue;
                    }
                    for(String categorie : artefact.getCategories()) {
                        if(categorie.toLowerCase().equals(filterPattern) && !filteredList.contains(artefact)) {
                            filteredList.add(artefact);
                            continue;
                        }
                        if(categorie.toLowerCase().startsWith(filterPattern) && !filteredList.contains(artefact)) {
                            filteredList.add(artefact);
                            continue;
                        }
                    }
                }
                filteredList.addAll(0, priority);
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            SearchAdapter.this.result.clear();
            SearchAdapter.this.result.addAll((List)results.values);

            SearchAdapter.this.notifyDataSetChanged();
        }
    }
}
