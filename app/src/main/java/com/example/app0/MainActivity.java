package com.example.app0;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    final private CatolgDbHelper dbHelper = new CatolgDbHelper(this);
    private RecyclerView artefcats;
    private SwipeRefreshLayout refresh;
    private RecyclerViewAdapter listAdapter;
    private SectionAdapter adaptersection ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        registerForContextMenu(findViewById(R.id.artefacts));

        this.listAdapter = new RecyclerViewAdapter(this, this.dbHelper.getAllArtefact());
        this.adaptersection = AdapterGlobal.createCatoAdapter(this,
                MainActivity.this.dbHelper.getCategories(), this.listAdapter.catalog);
        this.artefcats = findViewById(R.id.artefacts);
        this.artefcats.setLayoutManager(new LinearLayoutManager(this));
        this.artefcats.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        this.artefcats.setAdapter(this.adaptersection);

        this.refresh = findViewById(R.id.refresh);
        this.refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // remplir la liste des artefacts si on refresh
                new UpdateAllItemsTask().execute();
            }
        });


        if(this.listAdapter.catalog.isEmpty()) {
            // remplir la liste des artefacts si la liste est vide
            new UpdateAllItemsTask().execute();
            this.refresh.setRefreshing(true);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        int id = menuItem.getItemId();

        // trier par categories
         if(id == R.id.trierCategories) {
            this.adaptersection = AdapterGlobal.createCatoAdapter(this,
                    MainActivity.this.dbHelper.getCategories(), this.listAdapter.catalog);
            this.artefcats.setAdapter(this.adaptersection);

        }
         // trier par ordre alphabitique
      else  if (id == R.id.trierAlphabitique) {
            this.adaptersection = AdapterGlobal.CreateAlphabiTrierAdapter(this, this.listAdapter.catalog);
            this.artefcats.setAdapter(this.adaptersection);
        }
         // trier par ordre Chronologique
        else if(id == R.id.TrierDate) {
            this.adaptersection = AdapterGlobal.createDateTrierAdapter(this, this.listAdapter.catalog);
            this.artefcats.setAdapter(this.adaptersection);
        }
         // aller au searchActivity
        else if(id == R.id.goTosearch) {
            Intent intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putParcelableArrayListExtra("list", (ArrayList<Artefact>)this.listAdapter.catalog);
            MainActivity.this.startActivityForResult(intent, 1000);
        }

        this.adaptersection.notifyItemRangeChanged(0, this.adaptersection.getItemCount());

        return super.onOptionsItemSelected(menuItem);
    }

    // update task
    class UpdateAllItemsTask extends AsyncTask {

        @Override
        protected Void doInBackground(Object[] objects) {
            try {
                ArrayList<Artefact> catalog = RequestHttp.fetchAllItems();
                MainActivity.this.dbHelper.synchronize(catalog);
                MainActivity.this.listAdapter.catalog = catalog;

                    MainActivity.this.adaptersection = AdapterGlobal.createCatoAdapter(
                            MainActivity.this, MainActivity.this.dbHelper.getCategories(),
                            MainActivity.this.listAdapter.catalog);

            } catch (IOException e) { e.printStackTrace(); }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);


            MainActivity.this.refresh.setRefreshing(false);
            MainActivity.this.artefcats.setAdapter(MainActivity.this.adaptersection);
            MainActivity.this.adaptersection.notifyDataSetChanged();
        }
    }





}
