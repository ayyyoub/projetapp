package com.example.app0;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class RequestHttp {
	

	//request pour l'utiliser pour récupérer catolog
	public static String request(Type reqType, String url, String[] headers, String data) throws IOException {
		URL u = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) u.openConnection();
		
		connection.setRequestMethod(reqType.toString());
		
		if(headers != null) {
			String field, value;
			for(String s : headers) {
				if(s != null) {
					field = s.substring(0, s.indexOf(':'));
					value = s.substring(s.indexOf(':') + 1);
					connection.setRequestProperty(field, value);
				}
			}
		}
			
		if(data != null) {
			connection.setDoOutput(true);
			OutputStream outputStream = connection.getOutputStream();
			byte[] b = data.getBytes("UTF-8");
			outputStream.write(b);
			outputStream.flush();
			outputStream.close();
		}
		
		System.out.println("Storing the response..");
		StringBuilder content;

		try (BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
			String line;
			content = new StringBuilder();
			while ((line = input.readLine()) != null) {
				content.append(line);
				content.append(System.lineSeparator());
			}
		} 
		finally {
			connection.disconnect();
		}
		
		return content.toString();
	}
	
	/**
	 * Send an HTTP request to a server (URL)
	 * @param r the HTTP request to execute
	 * @return a response from the server
	 * @throws IOException if the connection with the server cannot be established
	 */
	public static String exec(Request r) throws IOException {
		return RequestHttp.request(r.reqType, r.url, r.headers, r.data);
	}
	
	static public enum Type { GET, POST; }

	static public class Request {
		public Type reqType;
		public String url;
		public String[] headers;
		public String data;
		
		public Request(Type reqType, String url, String[] headers, String data) {
			this.reqType = reqType;
			this.url = url;
			this.headers = headers;
			this.data = data;
		}
		
		@Override
		public String toString() {
			try {
				return RequestHttp.exec(this);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "";	//in case of error
		}
	}
	
/////////////////





	//    synchroniser la bdd locale avec le catalogue de l'API web
	public static ArrayList<Artefact> fetchAllItems() throws IOException {
		URL searchCataloglistUrl = WebServiceUrl.buildSearchCatalog();
		String res = RequestHttp.request(RequestHttp.Type.GET, searchCataloglistUrl.toString(), null, null);
		InputStream is = new ByteArrayInputStream(res.getBytes("UTF-8"));


		JSONResponseHandlerCatalog jsonCatalog = new JSONResponseHandlerCatalog();
		jsonCatalog.readJsonStream(is);
		return jsonCatalog.getArtefacts();
	}

}

