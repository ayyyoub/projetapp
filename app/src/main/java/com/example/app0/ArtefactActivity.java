package com.example.app0;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;


public class ArtefactActivity extends AppCompatActivity {

    private TextView nom, marque, annee, categories, description, details, LastUpdate;
    private ImageView imgTitre , TousPictures;
    private int imgNb = 0 , maxNbId =0;

    private Button butImgSuivante ;
    private TextView textDescriptionImgBas;




    public Artefact artefact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artefact);

        artefact = getIntent().getParcelableExtra(Artefact.TAG);

        this.nom = findViewById(R.id.artefactName);
        this.imgTitre = findViewById(R.id.imgArtefact);
        this.marque = findViewById(R.id.artefactMarque);
        this.annee = findViewById(R.id.artefactAnnee);
        this.categories = findViewById(R.id.artefactCategories);
        this.description = findViewById(R.id.artefactDesc);
        this.details = findViewById(R.id.artefactDetails);
        this.LastUpdate = findViewById(R.id.artefactLastUpdate);
        this.TousPictures = findViewById(R.id.artefactpictures);
        this.textDescriptionImgBas = findViewById(R.id.labelDescrptionImg);

        // mise à jour de view
        this.updateView();

        maxNbId = artefact.getPictures().size();
        Log.d("hhhhhhhhhhhhhhh", "onCreate: "+ maxNbId) ;
        this.butImgSuivante =  findViewById(R.id.buttonPictures);
        // voir si artefact.getPictures() est vide pour change visibilty
        if(maxNbId ==0 || artefact.getPictures() == null) {
            this.TousPictures.setVisibility(View.INVISIBLE);
            this.textDescriptionImgBas.setText("Pas d'images disponibles");
            this.butImgSuivante.setVisibility(View.INVISIBLE);
        }
        else{ textDescriptionImgBas.setText(artefact.getPictures().get(imgNb).getDescription());}
        this.butImgSuivante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    // passer à l'autre image
                if (maxNbId != imgNb ) {
                    textDescriptionImgBas.setText(artefact.getPictures().get(imgNb).getDescription());
                    Glide.with(ArtefactActivity.this)
                            .load(artefact.getPictures().get(imgNb).getImageUrl())
                            .centerCrop()
                            .into(ArtefactActivity.this.TousPictures);
                    imgNb =imgNb+1 ;


                } else {


                    imgNb = 0;


                }
            }
        });

    }

    private void updateView() {
        this.nom.setText(this.artefact.getnom());
        this.marque.setText(this.artefact.getmarque());
        this.description.setText(this.artefact.getDesc());

        if(this.artefact.getannee() == Artefact.NULL_YEAR)
            this.annee.setText("");
        else
            this.annee.setText(String.valueOf(this.artefact.getannee()));

        String categories = "";
        if(!this.artefact.getCategories().isEmpty()) {
            int i = 0;
            for(; i < this.artefact.getCategories().size() - 1 ; ++i)
                categories += this.artefact.getCategories().get(i) + ", ";
            categories += this.artefact.getCategories().get(i);
        }
        this.categories.setText(categories);

        String details = "Détails techniques : \n";
        if(!this.artefact.getTechDetails().isEmpty()) {
            int i = 0;
            for(i = 0 ; i < this.artefact.getTechDetails().size() - 1 ; ++i)
                details += "-" + this.artefact.getTechDetails().get(i) + "\n";
            details += "-" + this.artefact.getTechDetails().get(i);
        }
        this.details.setText(details);

        if(!this.artefact.getPictures().isEmpty()) {
            // add picture to TousPictures

            Glide.with(this)
                    .load(this.artefact.getPictures().get(0).getImageUrl())
                    .centerCrop()
                    .into(this.TousPictures);
        }
        String lastUpdate = "Dernière mise à jour : " + this.artefact.getLastUpdate();
        this.LastUpdate.setText(lastUpdate);
        // add picture to imgTitre
        Glide.with(this)
                .load(this.artefact.getphoto())
                .centerCrop()
                .into(this.imgTitre);
    }




}

