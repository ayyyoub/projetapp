package com.example.app0;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView rvSearchedItems;
    private SearchAdapter searchAdapter;
    private List<Artefact> catalog;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        registerForContextMenu(findViewById(R.id.rvSearchedItems));

        this.catalog = getIntent().getParcelableArrayListExtra("list");
        Collections.sort(this.catalog, new Comparator<Artefact>() {
            @Override
            public int compare(Artefact i1, Artefact i2) {
                return i1.getnom().compareTo(i2.getnom());
            }
        });

        this.searchAdapter = new SearchAdapter(this, this.catalog);

        this.rvSearchedItems = findViewById(R.id.rvSearchedItems);
        this.rvSearchedItems.setLayoutManager(new LinearLayoutManager(this));
        this.rvSearchedItems.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        this.rvSearchedItems.setAdapter(this.searchAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        searchItem.expandActionView();
        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                SearchActivity.this.searchView.setIconified(false);
                SearchActivity.this.searchView.requestFocusFromTouch();
                return false;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                SearchActivity.this.finish();
                return false;
            }
        });

        this.searchView = (SearchView) searchItem.getActionView();
        this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            // si on écrit envoie query
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            // appel focntion de filtrage
            public boolean onQueryTextChange(String newText) {
                SearchActivity.this.searchAdapter.getFilter().filter(newText);
                return false;
            }
        });
        searchView.setIconifiedByDefault(true);
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(searchManager != null)
            this.searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        this.searchView.setIconifiedByDefault(false);

        return super.onCreateOptionsMenu(menu);
    }


}
