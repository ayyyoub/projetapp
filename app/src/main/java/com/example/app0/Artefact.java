package com.example.app0;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;


public class Artefact implements Parcelable {

    public static final String TAG = Artefact.class.getSimpleName();
    public static final int NULL_YEAR = 1;

    private long idColumn;

    private String Id;
    private String nom;
    private String photo;
    private String marque;
    private int annee;
    private ArrayList<Integer> timeFrame;
    private ArrayList<String> categories;
    private String desc;
    private ArrayList<Image> pictures;
    private ArrayList<String> techDetails;
    private String lastUpdate;

    public long getColumnId() { return idColumn; }
    public void setId(long id) { this.idColumn = id; }

    public String getId() { return Id; }
    public void setId(String webId) { this.Id = webId; }

    public String getnom() { return nom; }
    public void setnom(String name) { this.nom = name; }

    public String getphoto() { return this.photo; }
    public void setphoto(String url) { this.photo = url; }

    public String getmarque() { return marque; }
    public void setmarque(String brand) { this.marque = brand; }

    public int getannee() { return annee; }
    public void setannee(int year) { this.annee = year; }

    public ArrayList<Integer> getTimeFrame() { return timeFrame; }
    public void setTimeFrame(ArrayList<Integer> timeFrame) { this.timeFrame = timeFrame; }

    public ArrayList<String> getCategories() { return categories; }
    public void setCategories(ArrayList<String> categories) { this.categories = categories; }

    public String getDesc() { return desc; }
    public void setDesc(String desc) { this.desc = desc; }

    public ArrayList<Image> getPictures() { return pictures; }

    public ArrayList<String> getTechDetails() { return techDetails; }

    public String getLastUpdate() { return lastUpdate; }
    public void setLastUpdate() {
        Date currentTime = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yyyy HH:mm:ss");
        this.lastUpdate = dateFormat.format(currentTime);
    }

    public void clearAllLists() {
        this.timeFrame.clear();
        this.techDetails.clear();
        this.pictures.clear();
        this.categories.clear();
    }

    public Artefact() {
        this.Id = new String("");
        this.nom = new String("");
        this.photo = new String("");
        this.marque = new String("");
        this.desc = new String("");
        this.lastUpdate = new String("");
        this.annee = 1;

        this.timeFrame = new ArrayList<>();
        this.categories = new ArrayList<>();
        this.pictures = new ArrayList<>();
        this.techDetails = new ArrayList<>();
    }

    public Artefact(Artefact artefact) {
        this.idColumn = artefact.idColumn;
        this.Id = new String(artefact.Id);
        this.nom = new String(artefact.nom);
        this.photo = new String(artefact.photo);
        this.marque = new String(artefact.marque);
        this.annee = artefact.annee;
        this.timeFrame = new ArrayList<>(artefact.timeFrame);
        this.categories = new ArrayList<>(artefact.categories);
        this.desc = new String(artefact.desc);
        this.pictures = new ArrayList<>(artefact.pictures);
        this.techDetails = new ArrayList<>(artefact.techDetails);
        this.lastUpdate = new String(artefact.lastUpdate);
    }

    public Artefact(long idColumn, String Id, String nom, String photo, String marque, int annee,
                    ArrayList<Integer> timeFrame, ArrayList<String> categories, String desc,
                    ArrayList<Image> pictures, ArrayList<String> techDetails, String lastUpdate)
    {
        this.idColumn = idColumn;
        this.Id = Id;
        this.nom = nom;
        this.photo = photo;
        this.marque = marque;
        this.annee = annee;
        this.timeFrame = timeFrame;
        this.categories = categories;
        this.desc = desc;
        this.pictures = pictures;
        this.techDetails = techDetails;
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return this.nom+", "+this.marque +" ("+this.annee +")\t#" + this.Id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeLong(idColumn);
        dest.writeString(nom);
        dest.writeString(photo);
        dest.writeString(marque);
        dest.writeInt(annee);
        dest.writeList(timeFrame);
        dest.writeList(categories);
        dest.writeString(desc);

        dest.writeTypedList(pictures);

        dest.writeList(techDetails);
        dest.writeString(lastUpdate);
    }


    public static final Creator<Artefact> CREATOR = new Creator<Artefact>()
    {
        @Override
        public Artefact createFromParcel(Parcel source)
        {
            return new Artefact(source);
        }

        @Override
        public Artefact[] newArray(int size)
        {
            return new Artefact[size];
        }
    };

    public Artefact(Parcel in) {
        this.idColumn = in.readLong();
        this.nom = in.readString();
        this.photo = in.readString();
        this.marque = in.readString();
        this.annee = in.readInt();
        this.timeFrame = in.readArrayList(Integer.class.getClassLoader());
        this.categories = in.readArrayList(String.class.getClassLoader());
        this.desc = in.readString();

        if(this.pictures == null) { this.pictures = new ArrayList<>(); }
        in.readTypedList(this.pictures, Image.CREATOR);

        this.techDetails = in.readArrayList(String.class.getClassLoader());
        this.lastUpdate = in.readString();
    }


}
