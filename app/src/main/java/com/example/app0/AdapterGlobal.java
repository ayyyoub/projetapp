package com.example.app0;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.example.app0.SectionAdapter.*; //Section

public class AdapterGlobal {

    public static SectionAdapter createCatoAdapter(Context context, List<String> categories, List<Artefact> list) {
        //   ajouter les données à la section correspondante
        List<Artefact> itemsSortedByCategories = new ArrayList<>();
        List<Section> sections = new ArrayList<>();
        for(String str : categories) {
            sections.add(new Section(itemsSortedByCategories.size(), str));
            for(Artefact artefact : list) {
                if(artefact.getCategories().contains(str))
                    itemsSortedByCategories.add(artefact);
            }
        }

        RecyclerViewAdapter sectionlessAdapter = new RecyclerViewAdapter(context, itemsSortedByCategories);

        SectionAdapter.Section[] dummy = new SectionAdapter.Section[sections.size()];
        SectionAdapter adapter = new SectionAdapter(context,R.layout.section,R.id.section_text,sectionlessAdapter);
        adapter.setSections(sections.toArray(dummy));
        return adapter;
    }
    //   ajouter les données à la section correspondante
    public static SectionAdapter createDateTrierAdapter(Context context, List<Artefact> catalog) {
        //    créer un adapter avec les données
        RecyclerViewAdapter sectionlessAdapter = new RecyclerViewAdapter(context, catalog);
        sectionlessAdapter.sortArtefactChronologically();

        //    créer les sections
        List<Section> sections = AdapterGlobal.createSectionDateTrier(sectionlessAdapter.catalog);


        SectionAdapter.Section[] dummy = new SectionAdapter.Section[sections.size()];
        SectionAdapter adapter = new SectionAdapter(context,R.layout.section,R.id.section_text,sectionlessAdapter);
        adapter.setSections(sections.toArray(dummy));
        return adapter;
    }
    //   ajouter les données à la section correspondante
    private static List<Section> createSectionDateTrier(List<Artefact> sortedCatalog) {
        List<Section> sections = new ArrayList<Section>();
        List<Integer> cache = new ArrayList<Integer>();
        for(Artefact artefact : sortedCatalog) {
            Integer year = artefact.getannee();
            if(!cache.contains(year)) {
                sections.add(new Section(sortedCatalog.indexOf(artefact), String.valueOf(year)));
                cache.add(year);
            }
        }
        return sections;
    }

    public static SectionAdapter CreateAlphabiTrierAdapter(Context context, List<Artefact> catalog) {

        //    créer un adapter avec les données
        RecyclerViewAdapter sectionlessAdapter = new RecyclerViewAdapter(context, catalog);
        sectionlessAdapter.sortArtefactAlphabetic();

        //    créer les sections
        List<Section> sections = AdapterGlobal.createSectionsTrierAlphabi(sectionlessAdapter.catalog);


        SectionAdapter.Section[] dummy = new SectionAdapter.Section[sections.size()];
        SectionAdapter adapter = new SectionAdapter(context,R.layout.section,R.id.section_text,sectionlessAdapter);
        adapter.setSections(sections.toArray(dummy));
        return adapter;
    }

    private static List<Section> createSectionsTrierAlphabi(List<Artefact> sortedCatalog) {
        List<Section> sections = new ArrayList<Section>();
        List<String> cache = new ArrayList<String>();
        for(Artefact artefact : sortedCatalog) {
            String firstLetter = AdapterGlobal.getpremierAlphabet(artefact.getnom()).toUpperCase();
            if(!cache.contains(firstLetter)) {
                sections.add(new Section(sortedCatalog.indexOf(artefact), firstLetter));
                cache.add(firstLetter);
            }
        }
        return sections;
    }

    private static String getpremierAlphabet(String alphabet) {
        Pattern p = Pattern.compile("\\p{L}");
        Matcher m = p.matcher(alphabet);
        if (m.find())
            return m.group();
        return " ";
    }


}